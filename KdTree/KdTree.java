/**
 * Based on assignment specification set forth in:
 * http://coursera.cs.princeton.edu/algs4/assignments/kdtree.html
 */

import java.util.Iterator;

public class KdTree {
    public static void main(String[] args) {
        Point2D p1 = new Point2D(0.2, 0.2);
        Point2D p2 = new Point2D(0.8, 0.8);
        Point2D p3 = new Point2D(0.2, 0.8);
        Point2D p4 = new Point2D(0.8, 0.2);

        KdTree test = new KdTree();
        test.insert(p1);
        test.insert(p2);
        test.insert(p3);
        test.insert(p4);

        RectHV rect = new RectHV(0, 0, 1, 1);

        Iterable<Point2D> points = test.range(rect);
        Iterator<Point2D> iter = points.iterator();

        while (iter.hasNext())
            StdOut.print(iter.next());
    }

    private Node root; // root of BST
    private int size;

    private class Node {
        private Point2D point;
        private Node left, right;
        // if vertical == true, that means this node places children
        // in the left or right sub-node based on x-coordinate.
        private boolean vertical;
        private double xmin, xmax, ymin, ymax;

        public Node(Point2D point, Boolean vertical, Node parent) {
            size++;
            this.point = point;
            this.vertical = vertical;
            this.left = null;
            this.right = null;

            if (parent == null) {
                xmin = 0;
                ymin = 0;
                xmax = 1;
                ymax = 1;
            } else {
                xmin = parent.xmin;
                ymin = parent.ymin;
                xmax = parent.xmax;
                ymax = parent.ymax;
                if (vertical) {
                    if (point.y() < parent.point.y()) {
                        ymax = parent.point.y();
                    } else if (point.y() > parent.point.y()) {
                        ymin = parent.point.y();
                    }
                } else {
                    if (point.x() < parent.point.x()) {
                        xmax = parent.point.x();
                    } else if (point.x() > parent.point.x()) {
                        xmin = parent.point.x();
                    }
                }
            }
        }
    }

    public KdTree() // construct an empty set of points
    {
    }

    public boolean isEmpty() // is the set empty?
    {
        return size == 0;
    }

    public int size() // number of points in the set
    {
        return size;
    }

    public void insert(Point2D p) // add the point p to the set (if it is not
                                  // already in the set)
    {
        if (contains(p)) {
            return;
        }

        if (root == null)
            root = new Node(p, true, null);
        else
            insert(p, root);
    }

    private void insert(Point2D p, Node n) {
        if (n.vertical) {
            if (p.x() < n.point.x())
                insertLeft(p, n);
            else
                insertRight(p, n);

        } else {
            if (p.y() < n.point.y())
                insertLeft(p, n);
            else
                insertRight(p, n);
        }
    }

    private void insertLeft(Point2D p, Node n) {
        if (n.left == null) {
            n.left = new Node(p, !n.vertical, n);
        } else
            insert(p, n.left);
    }

    private void insertRight(Point2D p, Node n) {
        if (n.right == null) {
            n.right = new Node(p, !n.vertical, n);
        } else
            insert(p, n.right);
    }

    public boolean contains(Point2D p) // does the set contain the point p?
    {
        if (root == null)
            return false;
        return contains(p, root);
    }

    private boolean contains(Point2D p, Node n) {
        if (n.point.equals(p))
            return true;
        else {
            if (n.vertical) {
                if (p.x() < n.point.x())
                    return leftContains(p, n);
                else
                    return rightContains(p, n);
            } else {
                if (p.y() < n.point.y())
                    return leftContains(p, n);
                else
                    return rightContains(p, n);
            }
        }
    }

    private boolean leftContains(Point2D p, Node n) {
        if (n.left == null)
            return false;
        return contains(p, n.left);
    }

    private boolean rightContains(Point2D p, Node n) {
        if (n.right == null)
            return false;
        return contains(p, n.right);
    }

    public void draw() // draw all of the points to standard draw
    {
        draw(root);
    }

    private void draw(Node n) {
        StdDraw.setPenColor(StdDraw.BLACK);
        StdDraw.setPenRadius(.01);
        StdDraw.point(n.point.x(), n.point.y());
        StdDraw.setPenRadius();
        if (n.vertical) {
            StdDraw.setPenColor(StdDraw.RED);
            StdDraw.line(n.point.x(), n.ymin, n.point.x(), n.ymax);
        } else {
            StdDraw.setPenColor(StdDraw.BLUE);
            StdDraw.line(n.xmin, n.point.y(), n.xmax, n.point.y());
        }

        if (n.left != null)
            draw(n.left);
        if (n.right != null)
            draw(n.right);
    }

    public Iterable<Point2D> range(RectHV rect) // all points in the set that
    // are inside the rectangle
    {
        return new InnerPoints(rect);
    }

    private class InnerPoints implements Iterable<Point2D> {
        private SolutionNode first0;
        private SolutionNode current0;
        private RectHV rect;

        private class SolutionNode {
            SolutionNode next;
            Point2D point;
        }

        private boolean rectContains(Node n) {
            return rect.contains(n.point);
        }

        private void findRange() {
            findRange(root);
        }

        private void findRange(Node n) {
            if (rectContains(n)) {
                addSolution(n.point);
            }
            if (n.vertical) {
                if (rect.xmin() <= n.point.x() && n.point.x() <= rect.xmax()) {
                    checkBothSides(n);
                } else if (rect.xmax() <= n.point.x()) {
                    if (n.left != null)
                        findRange(n.left);
                } else if (n.point.x() <= rect.xmin()) {
                    if (n.right != null)
                        findRange(n.right);
                }
            } else {
                if (rect.ymin() <= n.point.y() && n.point.y() <= rect.ymax()) {
                    checkBothSides(n);
                } else if (rect.ymax() <= n.point.y()) {
                    if (n.left != null)
                        findRange(n.left);
                } else if (n.point.y() <= rect.ymin()) {
                    if (n.right != null)
                        findRange(n.right);
                }
            }
        }

        private void checkBothSides(Node n) {
            if (n.left == null && n.right == null)
                return;
            if (n.left != null)
                findRange(n.left);
            if (n.right != null)
                findRange(n.right);
        }

        private void addSolution(Point2D point) {
            SolutionNode tmp = new SolutionNode();
            tmp.point = point;
            tmp.next = null;
            if (first0 == null) {
                first0 = tmp;
                current0 = first0;
            } else {
                current0.next = tmp;
                current0 = tmp;
            }
        }

        public InnerPoints(RectHV rect) {
            this.rect = rect;
            if (root != null)
                findRange();
        }

        public Iterator<Point2D> iterator() {
            Iterator<Point2D> it = new Iterator<Point2D>() {
                private SolutionNode curr0 = first0;

                public boolean hasNext() {
                    return curr0 != null;
                }

                public Point2D next() {
                    Point2D p = curr0.point;
                    curr0 = curr0.next;
                    return p;
                }

                public void remove() {
                    throw new UnsupportedOperationException();
                }
            };
            return it;
        }
    }

    public Point2D nearest(Point2D p) // a nearest neighbor in the set to p;
                                      // null if set is empty
    {
        if (isEmpty())
            return null;
        return findNearest(p, root, root.point);
    }

    // find distance between a point and node
    private Double findSquaredDistance(Point2D p, Node n) {
        if (n.vertical) {
            if (n.ymin > p.y())
                return p.distanceSquaredTo(new Point2D(n.point.x(), n.ymin));

            if (n.ymax < p.y())
                return p.distanceSquaredTo(new Point2D(n.point.x(), n.ymax));

            return (p.x() - n.point.x()) * (p.x() - n.point.x());
        } else {
            if (n.xmin > p.x())
                return p.distanceSquaredTo(new Point2D(n.xmin, n.point.y()));

            if (n.xmax < p.x())
                return p.distanceSquaredTo(new Point2D(n.xmax, n.point.y()));

            return (p.y() - n.point.y()) * (p.y() - n.point.y());
        }
    }

    // find nearest point in the kd-tree to the given point p
    private Point2D findNearest(Point2D p, Node n, Point2D closestPoint) {
        if (n == null)
            return closestPoint;

        if (p.distanceSquaredTo(n.point) < p.distanceSquaredTo(closestPoint))
            closestPoint = n.point;

        if (n.vertical) {
            if (p.x() < n.point.x())
                return exploreLeft(p, n, closestPoint);
            else
                return exploreRight(p, n, closestPoint);
        } else {
            if (p.y() < n.point.y())
                return exploreLeft(p, n, closestPoint);
            else
                return exploreRight(p, n, closestPoint);
        }
    }

    private Point2D exploreLeft(Point2D p, Node n, Point2D closestPoint) {
        Point2D result = findNearest(p, n.left, closestPoint);

        if (!result.equals(closestPoint))
            closestPoint = result;

        return compareResult(n.right, p, n, closestPoint);
    }

    private Point2D exploreRight(Point2D p, Node n, Point2D closestPoint) {
        Point2D result = findNearest(p, n.right, closestPoint);

        if (!result.equals(closestPoint))
            closestPoint = result;

        return compareResult(n.left, p, n, closestPoint);
    }

    private Point2D compareResult(Node compare, Point2D p, Node n,
            Point2D closestPoint) {
        if (n == null)
            return closestPoint;

        if (closestPoint != null) {
            if (p.distanceSquaredTo(closestPoint) <= findSquaredDistance(p, n)) {
                return closestPoint;
            } else {
                Point2D result = findNearest(p, compare, closestPoint);
                if (!result.equals(closestPoint))
                    closestPoint = result;
                return closestPoint;
            }
        } else {
            return findNearest(p, compare, closestPoint);
        }
    }
}
