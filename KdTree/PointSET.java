/**
 * Based on assignment specification set forth in:
 * http://coursera.cs.princeton.edu/algs4/assignments/kdtree.html
 */

import java.util.*;

public class PointSET {
    public static void main(String[] args) {
        Point2D p1 = new Point2D(0.2, 0.2);
        Point2D p2 = new Point2D(0.8, 0.8);
        Point2D p3 = new Point2D(0.2, 0.8);
        Point2D p4 = new Point2D(0.8, 0.2);

        PointSET test = new PointSET();
        test.insert(p1);
        test.insert(p2);
        test.insert(p3);
        test.insert(p4);

        StdOut.println("NEAREST: " + test.nearest(new Point2D(1, 0)));

        RectHV rect = new RectHV(0.5, 0, 1, 1);

        Iterable<Point2D> points = test.range(rect);
        Iterator<Point2D> iter = points.iterator();

        while (iter.hasNext())
            StdOut.print(iter.next());
    }

    private SET<Point2D> set;

    public PointSET() // construct an empty set of points
    {
        set = new SET<Point2D>();
    }

    public boolean isEmpty() // is the set empty?
    {
        return set.isEmpty();
    }

    public int size() // number of points in the set
    {
        return set.size();
    }

    public void insert(Point2D p) // add the point p to the set (if it is not
                                  // already in the set)
    {
        if (set.contains(p))
            return;

        set.add(p);
    }

    public boolean contains(Point2D p) // does the set contain the point p?
    {
        return set.contains(p);
    }

    public void draw() // draw all of the points to standard draw
    {
        StdDraw.setPenColor(StdDraw.BLACK);
        StdDraw.setPenRadius(.01);
        Iterator<Point2D> iter = set.iterator();

        while (iter.hasNext()) {
            Point2D p = iter.next();
            StdDraw.point(p.x(), p.y());
        }
    }

    public Iterable<Point2D> range(RectHV rect) // all points in the set that
                                                // are inside the rectangle
    {
        InnerPoints inner = new InnerPoints(rect);
        return inner;
    }

    private class InnerPoints implements Iterable<Point2D> {
        private Node first;
        private Node current;

        private class Node {
            private Node next;
            private Point2D point;
        }

        public InnerPoints(RectHV rect) {
            Iterator<Point2D> iter = set.iterator();

            while (iter.hasNext()) {
                Point2D p = iter.next();
                if (rect.contains(p)) {
                    Node tmp = new Node();
                    tmp.point = p;
                    tmp.next = null;
                    if (first == null) {
                        first = tmp;
                        current = first;
                    } else {
                        current.next = tmp;
                        current = tmp;
                    }
                }
            }
        }

        public Iterator<Point2D> iterator() {
            Iterator<Point2D> it = new Iterator<Point2D>() {
                private Node curr0 = first;

                public boolean hasNext() {
                    return curr0 != null;
                }

                public Point2D next() {
                    Point2D p = curr0.point;
                    curr0 = curr0.next;
                    return p;
                }

                public void remove() {
                    throw new UnsupportedOperationException();
                }
            };
            return it;
        }
    }

    public Point2D nearest(Point2D p) // a nearest neighbor in the set to p;
                                      // null if set is empty
    {
        if (isEmpty())
            return null;

        Iterator<Point2D> iter = set.iterator();

        Double minDistance = null;
        Double distance = null;
        Point2D closest = null;
        Point2D point = null;

        if (iter.hasNext()) {
            point = iter.next();
            minDistance = p.distanceSquaredTo(point);
            closest = point;
        }

        while (iter.hasNext()) {
            point = iter.next();
            distance = p.distanceSquaredTo(point);

            if (distance < minDistance) {
                minDistance = distance;
                closest = point;
            }
        }
        return closest;
    }
}