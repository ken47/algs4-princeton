/**
 * Based on assignment specification set forth in:
 * http://coursera.cs.princeton.edu/algs4/assignments/wordnet.html
 */

import java.util.Iterator;

public class SAP {
    private Digraph graph;

    public SAP(Digraph graph) {
        this.graph = new Digraph(graph);
    }

    private void checkBounds(int v, int w) {
        if (v < 0 || v > graph.V() - 1)
            throw new java.lang.IndexOutOfBoundsException();

        if (w < 0 || w > graph.V() - 1)
            throw new java.lang.IndexOutOfBoundsException();
    }

    private void checkBounds(Iterable<Integer> v, Iterable<Integer> w) {
        Iterator<Integer> iter = v.iterator();
        while (iter.hasNext()) {
            int tmp = iter.next();
            if (tmp < 0 || tmp > graph.V() - 1)
                throw new java.lang.IndexOutOfBoundsException();
        }

        iter = w.iterator();
        while (iter.hasNext()) {
            int tmp = iter.next();
            if (tmp < 0 || tmp > graph.V() - 1)
                throw new java.lang.IndexOutOfBoundsException();
        }
    }

    // length of shortest ancestral path between v and w; -1 if no such path
    public int length(int v, int w) {
        checkBounds(v, w);

        BreadthFirstDirectedPaths bfpV = new BreadthFirstDirectedPaths(graph, v);
        BreadthFirstDirectedPaths bfpW = new BreadthFirstDirectedPaths(graph, w);
        Integer minDistance = -1;

        for (int i = 0; i < graph.V(); i++) {
            if (bfpV.hasPathTo(i) && bfpW.hasPathTo(i)) {
                int dist = bfpV.distTo(i) + bfpW.distTo(i);
                if (minDistance < 0 || dist < minDistance) {
                    minDistance = dist;
                }
            }
        }

        return minDistance;
    }

    // a common ancestor of v and w that participates in a shortest ancestral
    // path; -1 if no such path
    public int ancestor(int v, int w) {
        checkBounds(v, w);

        BreadthFirstDirectedPaths bfpV = new BreadthFirstDirectedPaths(graph, v);
        BreadthFirstDirectedPaths bfpW = new BreadthFirstDirectedPaths(graph, w);
        Integer minDistance = -1;
        Integer ancestor0 = -1;

        for (int i = 0; i < graph.V(); i++) {
            if (bfpV.hasPathTo(i) && bfpW.hasPathTo(i)) {
                int dist = bfpV.distTo(i) + bfpW.distTo(i);
                if (minDistance < 0 || dist < minDistance) {
                    minDistance = dist;
                    ancestor0 = i;
                }
            }
        }

        return ancestor0;
    }

    // length of shortest ancestral path between any vertex in v and any vertex
    // in w; -1 if no such path
    public int length(Iterable<Integer> v, Iterable<Integer> w) {
        checkBounds(v, w);

        BreadthFirstDirectedPaths bfpV = new BreadthFirstDirectedPaths(graph, v);
        BreadthFirstDirectedPaths bfpW = new BreadthFirstDirectedPaths(graph, w);
        Integer minDistance = -1;

        for (int i = 0; i < graph.V(); i++) {
            if (bfpV.hasPathTo(i) && bfpW.hasPathTo(i)) {
                int dist = bfpV.distTo(i) + bfpW.distTo(i);
                if (minDistance < 0 || dist < minDistance) {
                    minDistance = dist;
                }
            }
        }

        return minDistance;
    }

    // a common ancestor that participates in shortest ancestral path; -1 if no
    // such path
    public int ancestor(Iterable<Integer> v, Iterable<Integer> w) {
        checkBounds(v, w);

        BreadthFirstDirectedPaths bfpV = new BreadthFirstDirectedPaths(graph, v);
        BreadthFirstDirectedPaths bfpW = new BreadthFirstDirectedPaths(graph, w);
        Integer minDistance = -1;
        Integer ancestor0 = -1;

        for (int i = 0; i < graph.V(); i++) {
            if (bfpV.hasPathTo(i) && bfpW.hasPathTo(i)) {
                int dist = bfpV.distTo(i) + bfpW.distTo(i);
                if (minDistance < 0 || dist < minDistance) {
                    minDistance = dist;
                    ancestor0 = i;
                }
            }
        }

        return ancestor0;
    }

    public static void main(String[] args) {
        In in = new In("/home/kenster/dev/algs4/wordnet/digraph1.txt");
        Digraph G = new Digraph(in);
        SAP sap = new SAP(G);
        int length, ancestor;

        length = sap.length(3, 11);
        ancestor = sap.ancestor(3, 11);
        StdOut.printf("length = %d, ancestor = %d\n", length, ancestor);

        length = sap.length(9, 12);
        ancestor = sap.ancestor(9, 12);
        StdOut.printf("length = %d, ancestor = %d\n", length, ancestor);

        length = sap.length(7, 2);
        ancestor = sap.ancestor(7, 2);
        StdOut.printf("length = %d, ancestor = %d\n", length, ancestor);

        length = sap.length(1, 6);
        ancestor = sap.ancestor(1, 6);
        StdOut.printf("length = %d, ancestor = %d\n", length, ancestor);
    }
}