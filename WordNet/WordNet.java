/**
 * Based on assignment specification set forth in:
 * http://coursera.cs.princeton.edu/algs4/assignments/wordnet.html
 */

import java.util.Iterator;

public class WordNet {
    private ST<String, Node> st;
    private ST<Integer, String> synsets0;
    private SAP sap0;
    private int size;

    private class Node {
        private Integer val;
        private Node next;

        public Node(int val) {
            this.val = val;
        }
    }

    private void constructSynSets(String synsets) {
        st = new ST<String, Node>();
        synsets0 = new ST<Integer, String>();
        In syn = new In(synsets);
        while (syn.hasNextLine()) {
            size++;
            String line = syn.readLine();
            String[] parts = line.split(",");
            String[] nouns = parts[1].split(" ");
            for (int i = 0; i < nouns.length; i++) {
                if (st.contains(nouns[i])) {
                    Node tmp = st.get(nouns[i]);
                    Node first = tmp;
                    while (tmp.next != null) {
                        tmp = tmp.next;
                    }
                    tmp.next = new Node(Integer.parseInt(parts[0]));

                    st.put(nouns[i], first);
                } else {
                    st.put(nouns[i], new Node(Integer.parseInt(parts[0])));
                }

                synsets0.put(Integer.parseInt(parts[0]), parts[1]);
            }
        }
        syn.close();
    }

    // it seems that there are two ways to find a potential root
    // 1) find a line with only one number (i.e. no outgoing edges)
    // 2) discover which vertices aren't in the file
    // make sure that there is exactly 1 potential root.
    private void constructSap(String hypernyms) {
        Digraph graph = new Digraph(size);
        In hyp = new In(hypernyms);

        boolean[] checkRoot = new boolean[size];

        int potentialRoots = 0;

        while (hyp.hasNextLine()) {
            String line = hyp.readLine();
            String[] parts = line.split(",");
            Integer origin = Integer.parseInt(parts[0]);

            if (parts.length == 1)
                potentialRoots++;

            checkRoot[origin] = true;

            for (int i = 1; i < parts.length; i++) {
                graph.addEdge(origin, Integer.parseInt(parts[i]));
            }
        }
        hyp.close();

        // not "rooted"
        // hyponym id's may appear more than once in text file as a source
        // vertex

        // if all id's don't have at least 1 incoming or outgoing edge,
        // then it is impossible for graph to be rooted
        for (int i = 0; i < checkRoot.length; i++) {
            if (!checkRoot[i]) {
                potentialRoots++;
            }
        }

        if (potentialRoots != 1)
            throw new java.lang.IllegalArgumentException();

        KosarajuSharirSCC testDag = new KosarajuSharirSCC(graph);
        if (testDag.count() > size)
            throw new java.lang.IllegalArgumentException();

        sap0 = new SAP(graph);
    }

    // constructor takes the name of the two input files
    public WordNet(String synsets, String hypernyms) {
        constructSynSets(synsets);
        constructSap(hypernyms);
    }

    // returns all WordNet nouns
    public Iterable<String> nouns() {
        return st.keys();
    }

    // is the word a WordNet noun?
    public boolean isNoun(String word) {
        return st.contains(word);
    }

    // distance between nounA and nounB (defined below)
    public int distance(String nounA, String nounB) {
        if (!isNoun(nounA) || !isNoun(nounB))
            throw new java.lang.IllegalArgumentException();

        Iterable<Integer> it0 = getIter(nounA);
        Iterable<Integer> it1 = getIter(nounB);

        return sap0.length(it0, it1);
    }

    private Iterable<Integer> getIter(final String noun) {
        return new Iterable<Integer>() {
            public Iterator<Integer> iterator() {
                return new Iterator<Integer>() {
                    Node curr0 = st.get(noun);

                    public boolean hasNext() {
                        return curr0 != null;
                    }

                    public Integer next() {
                        Integer val = curr0.val;
                        curr0 = curr0.next;
                        // StdOut.println(val);
                        return val;
                    }

                    public void remove() {
                        throw new UnsupportedOperationException();
                    }
                };
            }
        };
    }

    // a synset (second field of synsets.txt) that is the common ancestor of
    // nounA and nounB
    // in a shortest ancestral path (defined below)
    public String sap(String nounA, String nounB) {
        if (!isNoun(nounA) || !isNoun(nounB))
            throw new java.lang.IllegalArgumentException();

        Iterable<Integer> it0 = getIter(nounA);
        Iterable<Integer> it1 = getIter(nounB);

        return synsets0.get(sap0.ancestor(it0, it1));
    }

    // for unit testing of this class
    public static void main(String[] args) {
        // synsets15.txt; hypernyms = hypernymsPath15.txt
        WordNet wordnet = new WordNet(
                "/home/kenster/dev/algs4/wordnet/synsets3.txt",
                "/home/kenster/dev/algs4/wordnet/hypernymsInvalidCycle.txt");

        StdOut.println("23 white_marlin, mileage: "
                + wordnet.distance("white_marlin", "mileage"));
        StdOut.println("32 Black_Plague, black_marlin: "
                + wordnet.distance("Black_Plague", "black_marlin"));
        StdOut.println("32 American_water_spaniel, histology: "
                + wordnet.distance("American_water_spaniel", "histology"));
        StdOut.println("32 Brown_Swiss, barrel_roll: "
                + wordnet.distance("Brown_Swiss", "barrel_roll"));
    }
}