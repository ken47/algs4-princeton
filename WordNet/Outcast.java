/**
 * Based on assignment specification set forth in:
 * http://coursera.cs.princeton.edu/algs4/assignments/wordnet.html
 */

public class Outcast {
    private WordNet wordnet0;

    // constructor takes a WordNet object
    public Outcast(WordNet wordnet) {
        wordnet0 = wordnet;
    }

    // given an array of WordNet nouns, return an outcast
    public String outcast(String[] nouns) {
        Integer maxTotal = null;
        String theOutcast = null;

        for (int i = 0; i < nouns.length; i++) {
            int total = 0;
            for (int j = 0; j < nouns.length; j++) {
                total = total + wordnet0.distance(nouns[i], nouns[j]);
            }

            if (maxTotal == null || total > maxTotal) {
                maxTotal = total;
                theOutcast = nouns[i];
            }
        }

        return theOutcast;
    }

    public static void main(String[] args) {
        // synsets.txt hypernyms.txt outcast5.txt outcast8.txt outcast11.txt
        WordNet wordnet = new WordNet(
                "/home/kenster/dev/algs4/wordnet/synsets.txt",
                "/home/kenster/dev/algs4/wordnet/hypernyms.txt");
        Outcast outcast = new Outcast(wordnet);
        String[] nouns;

        nouns = In.readStrings("/home/kenster/dev/algs4/wordnet/outcast5.txt");
        StdOut.println(outcast.outcast(nouns));

        nouns = In.readStrings("/home/kenster/dev/algs4/wordnet/outcast8.txt");
        StdOut.println(outcast.outcast(nouns));

        nouns = In.readStrings("/home/kenster/dev/algs4/wordnet/outcast11.txt");
        StdOut.println(outcast.outcast(nouns));
    }
}