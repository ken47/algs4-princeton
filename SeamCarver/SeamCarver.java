/**
 * Based on assignment specification set forth in:
 * http://coursera.cs.princeton.edu/algs4/assignments/seamCarving.html
 */

import java.awt.Color;

public class SeamCarver {
    private Picture current;
    private int[][] vertexTo, energies, tmpEnergies, weights;

    public SeamCarver(Picture picture) {
        current = picture;
        setEnergies();
    }

    public Picture picture() // current picture
    {
        return current;
    }

    // cache energies since this is one of the
    // most expensive / repetitive parts of calculation
    private void setEnergies() {
        energies = new int[width()][height()];
        for (int x = 0; x < width(); x++) {
            for (int y = 0; y < height(); y++) {
                // these energies are all integer values
                // in this exercise, and ints require much
                // less space than doubles
                energies[x][y] = getIntEnergy(x, y);
            }
        }
    }

    public double energy(int x, int y) // energy of pixel at column x and row y
    {
        isPixelValid(x, y);
        return energies[x][y];
    }

    private int getCachedIntEnergy(int x, int y) {
        return energies[x][y];
    }

    private int getIntEnergy(int x, int y) {
        Double tmp = getEnergy(x, y);
        return tmp.intValue();
    }

    private double getEnergy(int x, int y) // energy of pixel at column x and
                                           // row y
    {
        isPixelValid(x, y);
        if (isBorder(x, y)) {
            return 195075.0;
        } else {
            int r, g, b;
            Color x0 = current.get(x - 1, y);
            Color x1 = current.get(x + 1, y);

            r = Math.abs(x0.getRed() - x1.getRed());
            g = Math.abs(x0.getGreen() - x1.getGreen());
            b = Math.abs(x0.getBlue() - x1.getBlue());

            double xGradient = r * r + g * g + b * b;
            Color y0 = current.get(x, y - 1);
            Color y1 = current.get(x, y + 1);

            r = Math.abs(y0.getRed() - y1.getRed());
            g = Math.abs(y0.getGreen() - y1.getGreen());
            b = Math.abs(y0.getBlue() - y1.getBlue());

            double yGradient = r * r + g * g + b * b;

            return xGradient + yGradient;
        }
    }

    public int width() // width of current picture
    {
        return current.width();
    }

    public int height() // height of current picture
    {
        return current.height();
    }

    private void isPixelValid(int x, int y) {
        if (x < 0 || y < 0 || x >= current.width() || y >= current.height())
            throw new java.lang.IndexOutOfBoundsException();
    }

    private boolean isBorder(int x, int y) {
        return (x == 0 || y == 0 || x == width() - 1 || y == height() - 1);
    }

    public int[] findHorizontalSeam() // sequence of indices for horizontal seam
    {
        int[] hSeam;
        hSeam = new int[width()];

        // special case if image is 2 pixels wide
        if (width() < 3) {
            for (int i = 0; i < height(); i++)
                hSeam[i] = 0;

            return hSeam;
        }

        weights = new int[width() - 1][height()];
        vertexTo = new int[width() - 1][height()];

        Integer min = Integer.MAX_VALUE;
        // since all border pixels have same energy, can be disregarded
        for (int x = 0; x < width() - 1; x++) {
            for (int y = 0; y < height(); y++) {
                if (x == 0) {
                    vertexTo[x][y] = 0;
                    weights[x][y] = 0;
                } else {
                    if (y - 1 >= 0) {
                        min = weights[x - 1][y - 1];
                        vertexTo[x][y] = y - 1;
                    }
                    if (weights[x - 1][y] < min) {
                        min = weights[x - 1][y];
                        vertexTo[x][y] = y;
                    }
                    if (y + 2 < height() && weights[x - 1][y + 1] < min) {
                        min = weights[x - 1][y + 1];
                        vertexTo[x][y] = y + 1;
                    }

                    weights[x][y] = min + getCachedIntEnergy(x, y);
                }
            }
        }

        // index of the second to last column
        // the last column consists of border pixels, with equal energy weights
        // so we can ignore it

        // finding the lowest weight in the final column
        int finalCol = width() - 2;
        min = Integer.MAX_VALUE;
        Integer minY = null;
        for (int i = 0; i < height() - 1; i++) {
            if (weights[finalCol][i] < min) {
                min = weights[finalCol][i];
                minY = i;
            }
        }
        if (minY - 1 >= 0)
            hSeam[width() - 1] = minY - 1;
        else
            hSeam[width() - 1] = minY;

        hSeam[width() - 2] = minY;

        // trace the seam
        for (int i = width() - 3; i >= 0; i--)
            hSeam[i] = vertexTo[i + 1][hSeam[i + 1]];

        return hSeam;
    }

    public int[] findVerticalSeam() // sequence of indices for vertical seam
    {
        int[] vSeam;
        vSeam = new int[height()];

        if (height() < 3) {
            for (int i = 0; i < width(); i++)
                vSeam[i] = 0;

            return vSeam;
        }

        weights = new int[width()][height() - 1];
        vertexTo = new int[width()][height() - 1];

        Integer min = Integer.MAX_VALUE;
        for (int y = 0; y < height() - 1; y++) {
            for (int x = 0; x < width(); x++) {
                if (y == 0) {
                    vertexTo[x][y] = 0;
                    weights[x][y] = 0;
                } else {
                    if (x - 1 >= 0) {
                        min = weights[x - 1][y - 1];
                        vertexTo[x][y] = x - 1;
                    }
                    if (weights[x][y - 1] < min) {
                        min = weights[x][y - 1];
                        vertexTo[x][y] = x;
                    }
                    if (x + 2 < width() && weights[x + 1][y - 1] < min) {
                        min = weights[x + 1][y - 1];
                        vertexTo[x][y] = x + 1;
                    }

                    weights[x][y] = min + getCachedIntEnergy(x, y);
                }
            }
        }

        int finalRow = height() - 2;
        min = Integer.MAX_VALUE;
        Integer minX = null;
        for (int i = 0; i < width() - 1; i++) {
            if (weights[i][finalRow] < min) {
                min = weights[i][finalRow];
                minX = i;
            }
        }

        if (minX - 1 >= 0)
            vSeam[height() - 1] = minX - 1;
        else
            vSeam[height() - 1] = minX;

        vSeam[height() - 2] = minX;

        for (int i = height() - 3; i >= 0; i--)
            vSeam[i] = vertexTo[vSeam[i + 1]][i + 1];

        return vSeam;
    }

    private void isValidHSeam(int[] seam) {
        if (seam.length != width()) {
            throw new java.lang.IllegalArgumentException();
        }

        Integer prev = seam[0];
        for (int i = 0; i < width(); i++) {
            if (Math.abs(seam[i] - prev) > 1)
                throw new java.lang.IllegalArgumentException();
            prev = seam[i];
        }
    }

    private void isValidVSeam(int[] seam) {
        if (seam.length != height())
            throw new java.lang.IllegalArgumentException();

        Integer prev = seam[0];
        for (int i = 0; i < height(); i++) {
            if (Math.abs(seam[i] - prev) > 1)
                throw new java.lang.IllegalArgumentException();
            prev = seam[i];
        }
    }

    public void removeHorizontalSeam(int[] a) // remove horizontal seam from
                                              // picture
    {
        isValidHSeam(a);
        if (width() == 0 || height() == 0)
            throw new java.lang.IllegalArgumentException();

        Picture tmpPic = new Picture(width(), height() - 1);
        for (int x = 0; x < width(); x++) {
            int yCount = 0;
            for (int y = 0; y < height(); y++) {
                if (y == a[x])
                    continue;
                tmpPic.set(x, yCount++, current.get(x, y));
            }
        }
        current = tmpPic;
        tmpEnergies = new int[width()][height()];
        for (int x = 0; x < width(); x++) {
            for (int y = 0; y < height(); y++) {
                if (a[x] - 1 == y)
                    tmpEnergies[x][a[x] - 1] = getIntEnergy(x, a[x] - 1);
                else if (a[x] == y) {
                    tmpEnergies[x][a[x]] = getIntEnergy(x, a[x]);
                } else if (a[x] < y) {
                    tmpEnergies[x][y] = energies[x][y + 1];
                } else {
                    tmpEnergies[x][y] = energies[x][y];
                }
            }
        }
        energies = tmpEnergies;
    }

    public void removeVerticalSeam(int[] a) // remove vertical seam from picture
    {
        isValidVSeam(a);
        if (width() == 0 || height() == 0)
            throw new java.lang.IllegalArgumentException();

        Picture tmpPic = new Picture(width() - 1, height());
        for (int y = 0; y < height(); y++) {
            int xCount = 0;
            for (int x = 0; x < width(); x++) {
                if (x == a[y])
                    continue;
                tmpPic.set(xCount++, y, current.get(x, y));
            }
        }
        current = tmpPic;
        tmpEnergies = new int[width()][height()];
        for (int y = 0; y < height(); y++) {
            for (int x = 0; x < width(); x++) {
                if (a[y] - 1 == x)
                    tmpEnergies[a[y] - 1][y] = getIntEnergy(a[y] - 1, y);
                else if (a[y] == x) {
                    tmpEnergies[a[y]][y] = getIntEnergy(a[y], y);
                } else if (a[y] < x) {
                    tmpEnergies[x][y] = energies[x + 1][y];
                } else {
                    tmpEnergies[x][y] = energies[x][y];
                }
            }
        }
        energies = tmpEnergies;
    }

    public static void main(String[] args) {
    }
}